package org.blinksd.utils;

public enum SettingCategory {
    GENERAL,
    KBD_LAYOUT,
    POPUP,
    TOP_BAR,
    BOTTOM_BAR,
    THEMING,
    THEMING_ADVANCED,
}
