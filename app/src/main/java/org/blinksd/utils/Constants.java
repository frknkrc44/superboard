package org.blinksd.utils;

public final class Constants {

    static final int MIN_LONG_PRESS_DURATION = 1;
    static final int MAX_LONG_PRESS_DURATION = 3;
    static final int MAX_VIBRATION_DURATION = 100;
    static final int MAX_RADIUS = 100;
    static final int MIN_TEXT_SIZE = 6;
    static final int MAX_TEXT_SIZE = 60;
    static final int MIN_KEYBOARD_HEIGHT = 10;
    static final int MAX_KEYBOARD_HEIGHT = 80;
    static final int MAX_OTHER_VAL = 40;
    static final int MIN_ICON_MULTI = 1;
    static final int MAX_ICON_MULTI = 10;
    static final int MIN_DICT_LIMIT = 3;
    static final int MAX_DICT_LIMIT = 50;
    static final int MAX_INDICATOR_HEIGHT = 20;
    static final int MIN_COMPAT_MONET_COLOR = 6;
    static final int MAX_COMPAT_MONET_COLOR = 128;

}
